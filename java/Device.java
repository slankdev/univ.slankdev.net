/* Device.java */
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


/**
 * デバイス管理用クラス。
 * IPアドレスと、ポート状態、生存状態をメンバとして持っています。
 * @author 城倉弘樹
 * @version 0.00
 */
public class Device {
	/**ポートの最大数*/
	private final int MAXPORTS = 1000;
	/**生存状態*/
	public boolean live;
	/**ポート状態*/
	public boolean ports[] = new boolean[MAXPORTS];
	/**IPアドレス*/
	public Ipaddr addr = new Ipaddr();
	
	
	/**
	 * 指定したIPアドレスを持つインスタンスを生成するコンストラクタ
	 * @param str　セットするIPアドレスの文字列
	 */
	public Device(String str){
		addr.set(str);
		live = addr.isLive(1);
	}
	
	/**
	 * IPアドレスと生存状態を格納したインスタンスを生成するコンストラクタ
	 * @param iaddr　IPアドレス
	 * @param ilive　生存状態　(t:live, f:die)
	 */
	public Device(Ipaddr iaddr, boolean ilive) {
		addr = iaddr.clone();
		live = ilive;
	}
	
	
	/**
	 * ポートがオープン化調べる
	 * 引数で指定したポートがオープンならばtrue, closeならfalseを返す
	 * @param iport　指定するポート番号
	 * @return　ポートが開いているかの真偽値 (t:open, f:close)
	 */
	public boolean hasOpenPort(int iport){
		String str = addr.toString();
		
		try {
			Socket sock = new Socket(str, iport);
			sock.close();
			return true;
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			return false;
		} 
	}
	
	
}
