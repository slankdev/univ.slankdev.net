/* MyFrame.java */
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import javax.swing.JLabel;
import javax.swing.JScrollPane;


public class MyFrame extends JFrame implements ActionListener {
	public static final long serialVersionUID = 1L;	
	private HeavyThread hvyThread;
	
	public JLabel      logLabel;
	public JButton     btn_logClear;
	public JTextArea   logarea;
	public JPanel      panel_ping;
	public JTextField  ping_mask;
	public JTextField  ping_ip;
	public JTextArea   ping_textarea;
	public JButton     ping_btnScan;
	public JButton     ping_btnStop;
	public JButton     ping_btnClear;
	public JPanel      panel_port;
	public JTextField  port_ip;
	public JTextArea   port_textarea;
	public JButton     port_btnScan;
	public JButton     port_btnStop;
	public JButton     port_btnClear;
	public JScrollPane scrollPane_log;
	public JScrollPane scrollPane_ping;
	public JScrollPane scrollPane_port;
	public JTabbedPane tabbedPane;
	


	public static void main(String[] args){
		MyFrame frame = new MyFrame("Network Tools version 0.00");
		redirectConsole(frame.logarea);
	    frame.setVisible(true);
	}
	public static void redirectConsole(JTextArea textarea) {
	    final ByteArrayOutputStream bytes = new ByteArrayOutputStream() {
	        public synchronized void flush() throws IOException {
	            textarea.setText(toString());
	        }
	    };
	    PrintStream out = new PrintStream(bytes, true);
	    System.setErr(out);
	    System.setOut(out);
	}

	
	public MyFrame(String title){
		setTitle(title);
	    setSize(799,451);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		logarea         = new JTextArea();
		logLabel          = new JLabel("log area");
		btn_logClear    = new JButton("clear");
		tabbedPane      = new JTabbedPane(JTabbedPane.TOP);
		panel_ping      = new JPanel();
		panel_port      = new JPanel();
		ping_ip         = new JTextField();
		ping_mask       = new JTextField();
		ping_btnScan    = new JButton("scan");
		ping_btnStop    = new JButton("stop");
	    ping_btnClear   = new JButton("clear");
		port_ip         = new JTextField();
		port_btnScan    = new JButton("scan");
		port_btnStop    = new JButton("stop");
	    port_btnClear   = new JButton("clear");
		scrollPane_log  = new JScrollPane(logarea);
		ping_textarea   = new JTextArea();
		scrollPane_ping = new JScrollPane(ping_textarea);
		port_textarea   = new JTextArea();
		scrollPane_port = new JScrollPane(port_textarea);
		
		logarea.setBounds(567, 36, 226, 372);
		logarea.setEditable(false);
		scrollPane_log.setBounds(567, 36, 226, 372);
		getContentPane().add(scrollPane_log);
		logLabel.setBounds(576, 17, 61, 16);
		btn_logClear.setBounds(676, 12, 117, 29);
		getContentPane().add(btn_logClear);
		btn_logClear.addActionListener(this);
	    btn_logClear.setActionCommand("log clear");

		tabbedPane.setBorder(null);
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedPane.setBounds(0, 0, 568, 423);
		getContentPane().add(tabbedPane);

		panel_ping.setLayout(null);
		panel_port.setLayout(null);

		ping_ip.setText("10.128.5.157");
		ping_ip.setBounds(6, 5, 139, 30);		
		ping_mask.setText("255.255.252.0");
		ping_mask.setBounds(157, 5, 139, 30);
		ping_ip.setText("192.168.179.3");			// for HOME
		ping_mask.setText("255.255.255.0");			// for HOME

		ping_btnScan.setBounds(308, 7, 99, 29);
		ping_btnScan.addActionListener(this);
	    ping_btnScan.setActionCommand("ping scan");
		ping_btnStop.setBounds(419, 7, 56, 29);
	    ping_btnStop.addActionListener(this);
	    ping_btnStop.setActionCommand("ping stop");
	    ping_btnClear.setBounds(487, 7, 56, 29);
	    ping_btnClear.addActionListener(this);
	    ping_btnClear.setActionCommand("ping clear");
		
		panel_ping.add(scrollPane_ping);
		//panel_ping.add(ping_textarea);
	
		ping_textarea.setBounds(6, 46, 537, 325);
		ping_textarea.setEditable(false);
		scrollPane_ping.setBounds(6, 46, 537, 325);
		
		port_ip.setText("10.128.5.157");
		port_ip.setBounds(6, 5, 280, 29);
		port_ip.setText("192.168.179.3");		// for HOME

		port_btnScan.setBounds(308, 6, 85, 29);
		port_btnScan.addActionListener(this);
	    port_btnScan.setActionCommand("port scan");	  
	    port_btnStop.setBounds(405, 6, 62, 29);
	    port_btnStop.addActionListener(this);
	    port_btnStop.setActionCommand("port stop");
	    port_btnClear.setBounds(479, 6, 62, 29);
	    port_btnClear.addActionListener(this);
	    port_btnClear.setActionCommand("port clear");
		
		port_textarea.setBounds(6, 47, 535, 325);
		port_textarea.setEditable(false);
		scrollPane_port.setBounds(6, 47, 535, 325);
		panel_port.add(scrollPane_port);
		//panel_port.add(port_textarea);
	    
		tabbedPane.addTab("Ping Scan", null, panel_ping, null);
		panel_ping.add(ping_ip);
		panel_ping.add(ping_mask);
		panel_ping.add(ping_btnScan);
		panel_ping.add(ping_btnStop);
	    panel_ping.add(ping_btnClear);
	    
		tabbedPane.addTab("Port Scan", null, panel_port, null);
		panel_port.add(port_ip);
		panel_port.add(port_btnScan);
	    panel_port.add(port_btnStop);
	    panel_port.add(port_btnClear);
		
	}
	
	
	
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if(cmd.equals("ping scan")){
			action_pingScan();
		}
		else if(cmd.equals("ping stop")){
			action_pingStop();
		}
		else if(cmd.equals("ping clear")){
			action_pingClear();
		}
		else if(cmd.equals("port scan")){
			action_portScan();
		}
		else if(cmd.equals("port stop")){
			action_portStop();
		}
		else if(cmd.equals("port clear")){
			action_portClear();
		}
		else if(cmd.equals("log clear")){
			action_logClear();
		}
	}
	
	
	
	public void action_pingScan(){
		ping_textarea.setText("");
		hvyThread = new HeavyThread(ping_btnScan, this, 1);
		hvyThread.start();
	}
	
	
	
	public void action_portScan(){
		port_textarea.setText("");
		hvyThread = new HeavyThread(port_btnScan, this, 2);
		hvyThread.start();
	}
	
	
	
	public void action_pingStop(){
		hvyThread.safeStop();
	}
	public void action_portStop(){
		hvyThread.safeStop();
	}	
	public void action_logClear(){ logarea.setText(""); }
	public void action_pingClear(){ ping_textarea.setText(""); }
	public void action_portClear(){ port_textarea.setText(""); }
}
