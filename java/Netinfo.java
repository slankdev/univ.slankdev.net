/* Netinfo.java */

/**
 * ネットワーク情報用クラス。
 * ネットワークアドレス、ネットマスク、存在しうるアドレス範囲などを扱えます。 
 * @author 城倉弘樹
 * @version 0.00
 */
public class Netinfo {
	/**IPアドレス*/
	public Ipaddr ip = new Ipaddr();
	/**ネットマスク*/
	public Ipaddr mask = new Ipaddr();
	/**ネットワークアドレス*/
	public Ipaddr netaddr = new Ipaddr();
	/**存在しうる最小のアドレス*/
	public Ipaddr maximum = new Ipaddr();
	/**存在しうる最大のアドレス*/
	public Ipaddr minimum = new Ipaddr();
	
	
	/**
	 * デフォルトコンストラクタです。
	 * 使用しません。
	 */
	public Netinfo(){
		getInfo();
	}
	
	
	
	/**
	 * IPアドレスとネットマスクを指定してネットワーク情報を取得します。
	 * 取得する内容は、ネットワークアドレス、存在しうる最小のアドレス,
	 * 存在しうる最大のアドレスの三つです。
	 * @param iip　自分のIPアドレス
	 * @param imask　ネットマスク
	 */
	public Netinfo(String iip, String imask){
		ip.set(iip);
		mask.set(imask);
		getInfo();
	}

	
	
	/**
	 * 情報を取得します。
	 * あらかじめ指定されたIPアドレスとネットマスクからネットワーク情報を取得します。
	 * 取得する内容は、ネットワークアドレス、存在しうる最小のアドレス,
	 * 存在しうる最大のアドレスの三つです。
	 */
	public void getInfo(){
		if(ip.isEmpty() || mask.isEmpty()){
			System.err.println("Error!! ip or mask was not setten");
			System.exit(-1);
		}

		byte[] b = new byte[4];
		int[]  buf = new int[4];
		for(int i=0; i<4; i++){
			b[i] = (byte) mask.num[i];
			buf[i] = ~b[i] & 0xff;
			netaddr.num[i] = ip.num[i] & mask.num[i];
			maximum.num[i] = buf[i] | netaddr.num[i];
		}
		minimum = netaddr.clone();
		minimum.countNext();
		maximum.countPrevious();
	}




}