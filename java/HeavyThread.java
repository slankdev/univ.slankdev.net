/* HeavyThread.java */
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;



/**
 * スレッド処理用クラス。
 * Threadクラスを継承指定していて、pingスキャンとportスキャンの機能を含んでいる。
 * @author 城倉弘樹
 * @version 0.00
 */
public class HeavyThread extends Thread {
	/**スレッドの実行状態 (true:実行, false:中止または終了)*/
	private boolean running;
	/**スレッドの実行モード (1:pingスキャン, 2:portスキャン)*/
	private int mode = 0;
	/** コンストラクタで指定するボタン。
	 *  実行中は無効化されます。 */
	private JButton button;
	/** コンストラクタで指定するJFrameオブジェクト。
	 *  実行時の出力などで使用する。 */
	private MyFrame frame = new MyFrame("");
	
	
	/**
	 * 新たにスレッドインスタンスを作成するコンストラクタです。
	 * ボタンとフレームを渡して、GUIサポートをします。
	 * @param btn	イベントのボタンを指定します。
	 * @param frm	GUIのフレームを指定します。
	 * @param md	スレッド処理のモードを指定します。(1:ping, 2:port)
	 */
	public HeavyThread(JButton btn, MyFrame frm, int md){
		button = btn;
		button.setEnabled(false);
		frame = frm;
		mode = md;
	}
	
	
	/**
	 * 指定したテキストエリアに文字列を出力します。
	 * @param str	出力する文字列を指定します。
	 * @param area	出力先となるJTextAreaを指定します
	 */
	public void OutTextArea(String str, JTextArea area){
		area.append(str);
	}
	
	/**
	 * テスト用の重い処理をするスレッド
	 */
	public void heavyproc(){
		int i=0;
		for(boolean b=true; b; i++){
			i++;
			if(i>500000000)	b=false;
		}
		for(int h=0; h<1000000000; h++){
            for(boolean b=true; b; i++){
                i++;
                if(i>500000000)	b=false;
            }
		}
	}
	
	
	/**
	 * スレッドを安全に終了させます。
	 */
	public void safeStop(){
		running = false;
	}
	
	
	
	/**
	 * スレッドの実行処理メソッドです。
	 * コンストラクタで指定したモードのメソッドを呼び出します。
	 * Pingスキャンモードと、Portスキャンモードが指定できます。
	 */
	public void run(){
		running = true;
		
		switch(mode){	 
			case 1:
				scan1(frame.ping_ip.getText(), frame.ping_mask.getText());
				break;
			case 2:
				scan2(frame.port_ip.getText());
				break;
			default:
				System.out.println("[error!!] unknown mode!!");
				break;
		}
		
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                button.setEnabled(true);  //　in force button 
            }
        });
		running = false;
	}

	/**
	 * Pingスキャンを行うメソッドです。
	 * 生存確認はInetAddressクラスのisReachableメソッドを使用しています。
	 * 生存確認のタイムアウトは1secです。
	 * @param ip	自分のIPアドレス
	 * @param mask	サブネットマスク
	 */
	private void scan1(String ip, String mask){	// ping scan
		int scanCount = 10;
		int liveCount = 0;
		Netinfo info = new Netinfo(ip, mask);
		ArrayList<Device> list = new ArrayList<Device>();
		Ipaddr addr = new Ipaddr();
		
		OutTextArea("Network tool version 0.00\n", frame.ping_textarea);
		OutTextArea("--------------------------------------\n", frame.ping_textarea);
		OutTextArea("Scan type  : Ping Scan\n", frame.ping_textarea);
		OutTextArea("IP address : " + ip + "\n", frame.ping_textarea);
		OutTextArea("Netmask  : " + mask + "\n", frame.ping_textarea);
		OutTextArea("--------------------------------------\n", frame.ping_textarea);
		OutTextArea("Starting ping scan...\n\n", frame.ping_textarea);
	
		System.out.printf("Starting Ping Scan\n");
		System.out.printf("Set ip address %s\n", ip);
		System.out.printf("Set net mask %s\n", mask);
		
		
		
		int j=0;
		for(addr = info.minimum.clone(); j<scanCount; addr.countNext(), j++){
		//for(addr = info.minimum.clone(); addr.isLessThanOrEqual(info.maximum); addr.countNext()){
			if(running == false){
				System.out.println("Thread Stoped!!!");
				break;
			}
			
			System.out.printf("scaning %s...", addr.toString());
			if(addr.isLive(1)){
				list.add(new Device(addr.clone(), true));
				liveCount++;
				System.out.println("live");
			}
			else{
				list.add(new Device(addr.clone(), false));
				System.out.println("");
			}
		}
		System.out.printf("%d/%d hosts is up\n", liveCount ,list.size());
		
		for(int i=0; i<list.size(); i++){	
			if(list.get(i).live){
				OutTextArea(" *     UP       " + list.get(i).addr.toString() + "\n", frame.ping_textarea);
			}
		}
		OutTextArea("\n" + liveCount + " hosts is liveing\n", frame.ping_textarea);
	}
	
	
	/**
	 * Portスキャンを行うメソッドです。
	 * 調べるポートの範囲は1-600です。
	 * @param ip	ターゲットとなるIPアドレスを指定します。
	 */
	private void scan2(String ip){ // port scan
		int min = 1;
		int max = 600;	
		int openCount=0;
		Device dev = new Device(frame.port_ip.getText());
		
		if(!dev.live){
			System.out.println("[ERROR!!]:  Device is not living");
			return;
		}
		
		
		OutTextArea("Network Tool version 0.00\n", frame.port_textarea);
		OutTextArea("--------------------------------------\n", frame.port_textarea);
		OutTextArea("Scan Type  : Port Scan\n", frame.port_textarea);
		OutTextArea("Target IP address : " + dev.addr.toString() + "\n", frame.port_textarea);
		OutTextArea("Scan range: " + min + " to " + max + "\n", frame.port_textarea);
		OutTextArea("--------------------------------------\n", frame.port_textarea);
		OutTextArea("Starting Port Scan\n\n", frame.port_textarea);
		
		for(int i=min; i<=max; i++){
			if(running == false){
				System.out.println("Thread Stoped!!!");
				return ;
			}
			
			System.out.print("scaning port " + i + "...");
			if(dev.hasOpenPort(i)){
				dev.ports[i] = true;
				openCount++;
				System.out.println("open");
				
				OutTextArea(i + " open serveice\n", frame.port_textarea);
			}else{
				System.out.println("");
			}
		}		
		System.out.printf("%d/%d ports open\n", openCount, max-min+1);
		OutTextArea("\n" + openCount + " ports open on " + ip + "\n", frame.port_textarea);
	}
}
