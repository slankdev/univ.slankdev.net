/* Ipaddr.java */
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.regex.MatchResult;


/**
 * IPアドレスを扱うためのクラス。
 * アドレスの比較、加算、減算を定義している。
 * @author 城倉弘樹
 * @version 0.00
 */
public class Ipaddr{
	/**入力用のScannerオブジェクト*/
	private Scanner scanner;
	/**IPアドレスの各オクテッド*/
	public int[] num = new int[4];



	/**
	 * 空のIPアドレスインスタンスを作成するデフォルトコンストラクタです。
	 */
	public Ipaddr(){
		clear();
	}

	/**
	 * 指定した文字列のIPアドレスインスタンスを作成するコンストラクタです。
	 * @param s　指定する文字列。IPアドレスの文字列を指定する。
	 */
	public Ipaddr(String s){
		clear();
		set(s);
	}

	/**
	 * インスタンスの複製を生成する
	 * @return IPアドレスインスタンス
	 */
	public Ipaddr clone(){
		Ipaddr buf = new Ipaddr();
		for(int i=0; i<4; i++)
			buf.num[i] = num[i];

		return buf;
	}

	/**
	 * 内部のIPアドレスを文字列として返す。
	 * @return  文字列
	 */
	public String toString(){
		String str = null;
		str = num[0] + "." + num[1] + "." + num[2] + "." + num[3];
		return str;
	}

	/**
	 * 内部のIPアドレスを空(0.0.0.0)に設定します。
	 */
	private void clear(){
		for(int i=0; i<4; i++)
			num[i] = 0;
	}

	
	/**
	 * IPアドレスの加算を定義します。
	 * 例) "192.168.11.45".countNext() :  192.168.11.46
	 * 例) "10.110.255.255".countNext() :  10.111.0.0
	 */
	public void countNext(){
		for(int i=3; i>=0; i--){
			if(num[i] == 255){
				num[i] = 0;
			}else{
				num[i]++;
				return;
			}
		}
	}
	
	
	/**
	 * IPアドレスの減算を定義します。
	 * 例) "192.168.11.45".countPrevious() : 192.168.11.44
	 * 例) "10.111.0.0".countPrevious() : 10.110.255.255
	 */
	public void countPrevious(){
		for(int i=3; i>=0; i--){
			if(num[i] == 0){
				num[i] = 255;
			}else{
				num[i]--;
				return;
			}
		}
	}
	
	/**
	 * IPアドレスを設定する。
	 * @param s　IPアドレスに指定する文字列。
	 */
	public void set(String s){
		scanner = new Scanner(s);
		scanner.findInLine("(\\d+).(\\d+).(\\d+).(\\d+)");
		MatchResult res = scanner.match();
		for(int i=1; i<=res.groupCount(); i++){
			num[i-1] = Integer.parseInt(res.group(i));
		}
	}
	
	/**
	 * 設定されているアドレスの生存確認をする。
	 * あらかじめ指定されているアドレスが生存しているかを
	 * InetAddressクラスのisReachableメソッドを使い
	 * 調べます。
	 * @param sec　タイムアウト時間
	 * @return	生存状態の真偽値を返します (true:live, false:die)
	 */
	public boolean isLive(int sec){
		InetAddress addr = null;
		try {
			addr = InetAddress.getByName(toString());
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		try {
			if(addr.isReachable(sec * 1000)) {
				return true;
			}else{
				return false;
			}
		} catch (IOException e2) {
			System.out.println("catch the Exception!!!");
			e2.printStackTrace();
		}
		return false;
	}
	
	/**
	 * IPアドレスの比較演算isGreaterThanを定義します。
	 * 例）192.168.120.11 isGreaterThan 192.168.120.6  : true
	 * 例）192.168.10.23  isGreaterThan 192.168.101.23  : false
	 * @param addr 比較対象のIPアドレスを指定します。
	 * @return　比較結果の真偽値を返します。
	 */
	public boolean isGreaterThan(Ipaddr addr){
		for(int i=0; i<4; i++){
			if(num[i] == addr.num[i])
				continue;
			else
				return num[i] > addr.num[i];
		}
		return false;
	}
	
	
	/**
	 * IPアドレスの比較演算isGreaterThanOrEqualを定義します。
	 * 例）192.168.120.11 isGreaterThanOrEqual 192.168.120.11  : true
	 * 例）192.168.10.23  isGreaterThanOrEqual 192.168.11.23  : false
	 * @param addr 比較対象のIPアドレスを指定します。
	 * @return　比較結果の真偽値を返します。
	 */
	public boolean isGreaterThanOrEqual(Ipaddr addr){
		for(int i=0; i<4; i++){
			if(num[i] == addr.num[i])
				continue;
			else
				return num[i] >= addr.num[i];
		}
		return true;
	}


	/**
	 * IPアドレスの比較演算isLessThanを定義します。
	 * 例）192.168.120.11 isLessThan 192.168.120.6  : false
	 * 例）192.168.10.23  isLessThan 192.168.101.23  : true
	 * @param addr 比較対象のIPアドレスを指定します。
	 * @return　比較結果の真偽値を返します。
	 */
	public boolean isLessThan(Ipaddr addr){
		for(int i=0; i<4; i++){
			if(num[i] == addr.num[i])
				continue;
			else
				return num[i] < addr.num[i];
		}
		return false;
	}
	
	
	/**
	 * IPアドレスの比較演算isLessThanOrEqualを定義します。
	 * 例）192.168.120.11 isLessThanOrEqual 192.168.120.11  : true
	 * 例）192.168.11.23 isLessThanOrEqual 192.168.10.23  : true
	 * @param addr 比較対象のIPアドレスを指定します。
	 * @return　比較結果の真偽値を返します。
	 */
	public boolean isLessThanOrEqual(Ipaddr addr){
		for(int i=0; i<4; i++){
			if(num[i] == addr.num[i])
				continue;
			else
				return num[i] < addr.num[i];
		}
		return true;
	}
	
	/**
	 * IPアドレスの比較演算isSameAsを定義します。
	 * 例）192.168.120.11 isSameAs 192.168.120.11  : true
	 * 例）192.168.11.23  isSameAs 192.168.10.23  : false
	 * @param addr 比較対象のIPアドレスを指定します。
	 * @return　比較結果の真偽値を返します。
	 */
	public boolean isSameAs(Ipaddr addr){
		for(int i=0; i<4; i++){
			if(num[i] != addr.num[i])
				return false;
		}
		return true;
	}
	
		

	/**
	 * IPアドレスが空かどうか調べます。
	 * IPアドレスが空、とは0.0.0.0を示します。
	 * @return　空かどうかの真偽値
	 */
	public boolean isEmpty(){
		for(int i=0; i<4; i++)
			if(num[i] != 0)	return false;
		return true;
	}

}
