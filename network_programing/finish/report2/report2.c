/*
 *
 *  Report2  
 *  14x3103 Hiroki Shirokura
 *  http://slankdev.net
 *	
 *	工夫はとくになし
 *
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
 *	ipアドレスを文字列から取得する
 *	
 *	引数
 *		str
 *		取得もとの文字列
 *
 *		array
 *		入力内容を格納する配列
 *
 *	返り値なし
 *
 * */
void getaddrbystr(const char* str, int arry[4]){
	sscanf(str, "%d.%d.%d.%d", &arry[0], 
			&arry[1], &arry[2], &arry[3]);
	return ;
}



/*
 *	二進数の数値を取得する
 *
 *	引数
 *		num　数値
 *		str  二進数の文字列を格納する配列
 *	戻り値
 *		格納した文字列のポインタ
 *
 */
char* get_bin(int num, char* str){
	char buf[8];
	int tester =1;
	int i, k;

	for(i=0; i<8; i++){
		if((num & tester) == 0) buf[i] = '0';
		else				  buf[i] = '1';
		tester = tester << 1;
	}
	for(i=7, k=0; i>=0; i--, k++)
		str[k] = buf[i]; 

	str[k] = 0;
	return str;
}



int main(int argc, char** argv){
	if(argc != 2){
		fprintf(stderr, "Usage \n")	;
		return -1;
	}
	
	char buf[9];
	int array[4];
	
	getaddrbystr(argv[1], array);
	
	for(int i=0; i<4; i++){
		printf("%d", array[i]);
		if(i<3) printf(".");
		else    printf("\n");
	}
	
	for(int i=0; i<4; i++){
		printf("%s", get_bin(array[i], buf));
		if(i<3) printf(".");
		else    printf("\n");
	}
		
}
