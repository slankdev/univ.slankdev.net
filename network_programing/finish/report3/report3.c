/*
 *
 *  Report3
 *  14x3103 Hiroki Shirokura
 *  http://slankdev.net
 *
 *	工夫はとくになし
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>




/*
 *	ipアドレスを文字列から取得する
 *	
 *	引数
 *		str
 *		取得もとの文字列
 *
 *		array
 *		入力内容を格納する配列
 *
 *	返り値なし
 *
 * */
void getaddrbystr(const char* str, unsigned int arry[4]){
	sscanf(str, "%d.%d.%d.%d", &arry[0], 
			&arry[1], &arry[2], &arry[3]);
	return ;
}

/*
 * 配列からIPアドレスの文字列を取得
 *
 * 引数
 *  アドレスの格納された配列
 *
 * 返り値
 * 	文字列ポインタ
 *
 * */
char* getstrbyaddr(unsigned int arry[], char* buf){
	sprintf(buf, "%u.%u.%u.%u", (unsigned char)arry[0], 
			(unsigned char)arry[1], (unsigned char)arry[2], (unsigned char)arry[3]);
	return buf;
}




/*
 *	ネットマスクの配列からCIDR方式の数値を返す。
 *
 *	引数
 *		ネットマスクの配列
 *
 *	返り値
 *		プレフィッックス値
 * */
int getprefixbymask(unsigned int a[]){
	int i;
	int pref;
	unsigned char arry[4];
	
	for(i=0; i<4; i++)
		arry[i] = (unsigned char)a[i];

	for(i=0, pref=0; i<4; i++){
		for(; ; pref++){
			if(arry[i] == 0)
				break;
			else
				arry[i] <<= 1;
		}
	}
	return pref;	
}



int main(int argc, char** argv){

	if(argc != 3){
		fprintf(stderr, "Usage: ipaddr netmask \n")	;
		return -1;
	}
	char buf[32];
	unsigned int  addr[4];	
	unsigned int  mask[4];	
	unsigned int  net[4];
	unsigned int  bcast[4];

	memset(buf  , 0, sizeof(buf  ));
	memset(addr , 0, sizeof(addr ));
	memset(mask , 0, sizeof(mask ));
	memset(net  , 0, sizeof(net  ));
	memset(bcast, 0, sizeof(bcast));

	getaddrbystr(argv[1], addr);
	getaddrbystr(argv[2], mask);
	
	int i;
	for(i=0; i<4; i++){
		net[i]   = addr[i] & mask[i];
		bcast[i] = net[i]  | ~mask[i];
	}

	printf("IP address        : %s \n", getstrbyaddr(addr , buf));
	printf("Sub Netmask       : %s \n", getstrbyaddr(mask , buf));
	printf("Network address   : %s \n", getstrbyaddr(net  , buf));
	printf("Broadcast address : %s \n", getstrbyaddr(bcast, buf));
	printf("Prefix address    : %s/%d \n", 
			getstrbyaddr(addr , buf), getprefixbymask(mask));
}
