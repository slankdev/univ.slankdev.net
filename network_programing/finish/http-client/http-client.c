/*
 *
 *  simple HTTP client 
 *  14x3103 Hiroki Shirokura
 *  use C99 for abailable declare value inside for()
 *  http://slankdev.net
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>


/*
 * 受信したデータをファイルに書き込む関数、
 * ファイル名をしていして実行する。
 *
 * 引数
 * 		const char* f
 * 		保存するファイル名。
 * 		存在する場合は上書きする。
 * 		
 * 		void*       b
 *		書き込むデータの先頭ポインタ。
 *
 * 		int         l
 * 		書き込むデータ長。
 *
 * 返り値
 * 		成功した場合正の整数
 * 		失敗した場合負の整数
 */
int save(const char* f, void* b, int l){
	FILE* fp=fopen(f, "wb");
	if(fp == NULL){
		perror("fopen");
		return -1;
	}

	if(fwrite(b, l, 1, fp) != 1){
		perror("fwrite");
		fclose(fp);
		return -1;
	}
	return l;
}



/*
 * 受信したデータからHTTPのヘッダ部分のみを取り除く
 *
 * 引数
 * 		buf
 * 		読み取るデータの先頭ポインタ
 *
 * 		len
 * 		読み取るデータの長さ
 *
 * 返り値
 * 		ヘッダのデータのながさ
 */
int get_header_len(const void* buf, int len){
	const char str[] = {'\r', '\n', '\r', '\n'};
	
	for(int i=0; i<len; i++){
		if(memcmp(str, &buf[i], 4) == 0)
			return i+4;
	}
	return -1;
}




int main(int argc, char** argv){
	if(argc != 2){
		printf("usage: %s hostname \n", argv[0]);
		return -1;
	}
	
	char* p;
	int  len;
	char buf[10000];
	int  all_len;
	char all_buf[1000000];
	int sock;
	struct sockaddr_in sin;
	struct hostent *host;

	

	host = gethostbyname(argv[1]);
	if(host == NULL){
		herror("gethostbyname");
		return -1;
	}	

	// make socket 
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sock < 0){
		perror("socket");
		return -1;
	}

	// connect 
	sin.sin_family = AF_INET;
	sin.sin_port = htons(80);
	sin.sin_addr = *(struct in_addr*)(host->h_addr);
	if(connect(sock, (struct sockaddr*)&sin, sizeof(sin)) < 0){
		perror("connect");
		close(sock);
		return -1;
	}

	char send_buf[256];
	memset(send_buf, 0, sizeof(send_buf));
	sprintf(send_buf,
			"GET / HTTP/1.0\r\n"
			"Host: %s\r\n"
			"Connection: close\r\n\r\n", argv[1]);

	// データの書き込み
	fwrite(send_buf, strlen(send_buf), 1, stdout);
	
	// get request sending
	len = send(sock, send_buf, strlen(send_buf), 0);
	if(len < 0){
		perror("send");
		close(sock);
		return -1;
	}
	
	p = all_buf;
	memset(all_buf, 0, sizeof(all_buf));


	// recv HTTP responce
	while (len > 0) {
		memset(buf, 0, sizeof(buf));
		len = recv(sock, buf, sizeof(buf), 0);
		if (len < 0) {
			perror("recv");
			close(sock);
			return -1;
		}
		memcpy(p, buf, len);
		p += len;
	}
	all_len = p - all_buf;
	
	int header_len = get_header_len(all_buf, all_len);
	fwrite(all_buf, header_len, 1, stdout);
	save("index.html", all_buf+header_len, all_len-header_len);
}
