/*
 *
 *  socket client server program
 *  14x3103 Hiroki Shirokura
 *  use C99 for abailable declare value inside for()
 *  http://slankdev.net
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>




void handle_client(int);


int main(int argc, char** argv){
	if(argc != 2){
		printf("usage: %s port \n", argv[0]);
		return -1;
	}

	
	int sock;
	int port = atoi(argv[1]);
	struct sockaddr_in sin;

	// make socket
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sock < 0){
		perror("socket");
		return -1;
	}


	// connect
	// 3 way handshake
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	if(bind(sock, (struct sockaddr*)&sin, sizeof(sin)) < 0){
		perror("bind");
		close(sock);
		return -1;
	}
	

	// listen socket
	// 受信待機の事
	if(listen(sock, 5) < 0){
		perror("listen");
		close(sock);
		return -1;
	}
	printf("listening TCP/%d.... \n", port);

	int pid;
	int csock;
	struct hostent*    cent;
	struct sockaddr_in csin;
	int csin_len = sizeof(csin);
	while(1){
		// クライアントからのconnect要求を承認して、
		// そのクライアントと通信可能なソケットを確保
		csock = accept(sock, (struct sockaddr*)&csin, (socklen_t*)&csin_len);
		if(csock < 0){
			perror("accept");
			continue;
		}
		
		// 接続クライアント情報の取得
		cent = gethostbyaddr(&(csin.sin_addr.s_addr), 
				sizeof(csin.sin_addr.s_addr), AF_INET);
		if(cent == NULL){
			herror("gethostbyaddr");
			close(csock);
			continue;
		}
		printf("[connect]  by %s %s:%d\n", cent->h_name, 
				inet_ntoa(csin.sin_addr), csin.sin_port);
		
		//　新たにプロセスを作成してそこでクライアント処理
		pid = fork();
		if(pid == 0){		 /* child prosess */
		/*-----------------------------------------------------------*/
			
			handle_client(csock);
			close(csock);
			return 0;

		/*-----------------------------------------------------------*/
		}

	}
	
	close(sock);
}

/*
 *	クライアント処理用の関数
 *	クライアントの接続要求に対応する
 *	マルチクライアント対応させるため
 *
 * 引数
 * 	sock　クライアントソケット
 *
 *
 * 返り値なし
 *
 * */
void handle_client(int sock){
	char send_buf[256];
	char recv_buf[256];
	int len;

	
	while(1){
		len = recv(sock, recv_buf, sizeof(recv_buf), 0);
		if(len < 0){
			perror("recv");
			continue ;
		}
		recv_buf[len] = 0;



		printf("recv: %s \n", recv_buf);


		strcpy(send_buf, "OK");
		strcat(send_buf, recv_buf);


		len = send(sock, send_buf, len+2, 0);
		if(len < 0){
			perror("send");
			continue;
		}
	}

}
