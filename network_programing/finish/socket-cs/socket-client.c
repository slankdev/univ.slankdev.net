/*
 *
 *  socket client server program
 *  14x3103 Hiroki Shirokura
 *  use C99 for abailable declare value inside for()
 *  http://slankdev.net
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>



int main(int argc, char** argv){
	if(argc != 3){
		printf("usage: %s address port \n", argv[0]);
		return -1;
	}

	
	int  len;
	char buf[256];
	int sock;
	struct sockaddr_in server_sin;

	/* allocate socket */
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sock < 0){
		perror("socket");
		return -1;
	}

	/* 
	 * connect to server
	 * do 3 way handshake
	 */
	server_sin.sin_family = AF_INET;
	server_sin.sin_port = htons(atoi(argv[2]));
	inet_aton(argv[1], &(server_sin.sin_addr));
	if(connect(sock, (struct sockaddr*)&server_sin, sizeof(server_sin)) < 0){
		perror("connect");
		close(sock);
		return -1;
	}
	
	/*
	 *	一度で終了せずに何度でもエコー要求をできるように改良
	 *
	 * */
	while(1){
		memset(buf, 0, sizeof buf);
		printf("send message: ");
		fgets(buf, 42, stdin);
		buf[strlen(buf)-1] = 0;

		if(strlen(buf)==0)
			continue;


		len = send(sock, buf, strlen(buf), 0);
		if(len < 0){
			perror("send");
			close(sock);
			return -1;
		}

		len = recv(sock, buf, sizeof(buf), 0);
		if(len < 0){
			perror("recv");
			close(sock);
			return -1;
		}

		printf("recv message: %s \n", buf);
	}
}
