/*
 *
 *  Report1   
 *  14x3103 Hiroki Shirokura
 *  http://slankdev.net
 *	
 *
 *	工夫はとくになし
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>



/*
 *	二進数の数値を取得する
 *
 *	引数
 *		num　数値
 *		str  二進数の文字列を格納する配列
 *	戻り値
 *		格納した文字列のポインタ
 *
 */
char* get_bin(int num, char* str){
	char buf[32];
	int tester =1;
	int i, k;

	for(i=0; i<32; i++){
		if((num & tester) == 0) buf[i] = '0';
		else				  buf[i] = '1';
		tester = tester << 1;
	}
	for(i=31, k=0; i>=0; i--, k++)
		str[k] = buf[i]; 

	str[k] = 0;
	return str;
}




int main(int argc, char** argv){
	if(argc != 3){
		fprintf(stderr, "Usage: %s integer integer \n", argv[0]);
		return -1;
	}
	
	char buf[33];

	int a = atoi(argv[1]);
	int b = atoi(argv[2]);

	int and = a & b;
	int xor = a & b;
	int ra = a<<1;
	int rb = b<<1;
	
	printf("and(a,b)      Decimal=%-4d  Hex=%-4x     Binary=%s \n", and, and, get_bin(and, buf));	
	printf("xor(a,b)      Decimal=%-4d  Hex=%-4x     Binary=%s \n", xor, xor, get_bin(xor, buf));	
	printf("argv[1]<<1    Decimal=%-4d  Hex=%-4x     Binary=%s \n", ra , ra , get_bin(ra, buf));	
	printf("argv[2]<<1    Decimal=%-4d  Hex=%-4x     Binary=%s \n", rb , rb , get_bin(rb, buf));	
		
}


