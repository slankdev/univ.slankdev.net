
#ifndef SERVER_H
#define SERVER_H


#include <vector>
#include <string>
#include <stdio.h>



class user{
private:
	static int nextid;
public:
	int sock;
	char name[32];
	char partner[32];
	int  id;
	
	user(){
		id = nextid;
		nextid++;
	}
	void setname(const char* n){
		strncpy(name, n, sizeof(name)-1);
	}
	void setpartner(const char* n){
		strncpy(partner, n, sizeof(partner)-1);
	}
	void info(){
		printf("id=%d name=%s partner=%s \n", id, name, partner);
	}
};
int user::nextid = 0;


struct thread_arg{
	int sock;	
};




void chat(int , int);
int   get_user_list(std::vector<user> vec, char* buf, int len);
void* handle_client(void* user);


int shell( char*, std::vector<std::string>*);


#endif /* SERVER_H */
