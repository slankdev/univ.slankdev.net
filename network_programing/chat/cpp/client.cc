/*
 *
 *  Report2  
 *  14x3103 Hiroki Shirokura
 *  use C99 for abailable declare value inside for()
 *  http://slankdev.net
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include <pthread.h>
#include "head.h"



void* send_message(void*);

int main(int argc, char** argv){
	if(argc != 3){
		printf("usage: %s address port \n", argv[0]);
		return -1;
	}

	
	int  len;
	char buf[256];
	int sock;
	struct sockaddr_in sin;
	struct timeval timeout;
	struct thread_arg* arg;
	pthread_t thread_id;
	

	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sock < 0){
		perror("socket");
		return -1;
	}

		
	sin.sin_family = AF_INET;
	sin.sin_port = htons(atoi(argv[2]));
	inet_aton(argv[1], &(sin.sin_addr));


	if(connect(sock, (struct sockaddr*)&sin, sizeof(sin)) < 0){
		perror("connect");
		return -1;
	}




/*---------------------------------------------------------------------------*/
	printf("user name: ");
	fgets(buf, 42, stdin);
	buf[strlen(buf)-1] = 0;
	len = send(sock, buf, strlen(buf), 0);
	if(len < 0){
		perror("send");
		close(sock);
		return -1;
	}


	arg = (struct thread_arg*)malloc(sizeof(struct thread_arg));
	if(arg == NULL){
		perror("malloc");
		return -1;
	}
	arg->sock = sock;

	if(pthread_create(&thread_id, NULL, send_message, (void*)arg) != 0){
		perror("pthread");
		return -1;
	}



	while(1){

		memset(buf, 0, sizeof buf);
		len = recv(sock, buf, sizeof(buf), 0);
		if(len < 0){
			perror("recv");
			return -1;
		}
		printf("\r");
		printf("recv: %s \n", buf);

	}

	close(sock);

/*---------------------------------------------------------------------------*/
}




void* send_message(void* arg){
	char buf[256];
	int len;
	int sock = ((struct thread_arg*)arg)->sock;

	while(1){
		usleep(200);
			
		memset(buf, 0, sizeof buf);
		printf("send: ");
		fgets(buf, 42, stdin);
		buf[strlen(buf)-1] = 0;

		if(strlen(buf)==0)
			continue;
		len = send(sock, buf, strlen(buf), 0);
		if(len < 0){
			perror("send");
			continue;
		}
	}


	return NULL;	
}
