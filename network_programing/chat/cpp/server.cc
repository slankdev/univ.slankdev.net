/*
 *
 *  Report2  
 *  14x3103 Hiroki Shirokura
 *  use C99 for abailable declare value inside for()
 *  http://slankdev.net
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>

#include <pthread.h>
#include <poll.h>
#include <vector>
#include "head.h"






int send_members(std::vector<user>, int, const void*, int);


std::vector<user> alluser;



int main(int argc, char** argv){
	if(argc != 2){
		printf("usage: %s port \n", argv[0]);
		return -1;
	}
	
	int server_sock;
	int port = atoi(argv[1]);
	struct sockaddr_in sin;

	server_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(server_sock < 0){
		perror("socket");
		return -1;
	}

	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	if(bind(server_sock, (struct sockaddr*)&sin, sizeof(sin)) < 0){
		perror("bind");
		close(server_sock);
		return -1;
	}

	if(listen(server_sock, 5) < 0){
		perror("listen");
		close(server_sock);
		return -1;
	}
	printf("Staring slankchat server \n");
	printf("listening TCP/%d.... \n\n", port);



	int sock;
	struct sockaddr_in csin;
	int csin_len = sizeof(csin);
	struct thread_arg* arg;
	pthread_t thread_id;

	while(1){
/*---------------------------------------------------------------------------------*/
		
		sock = accept(server_sock, (struct sockaddr*)&csin, (socklen_t*)&csin_len);
		if(sock < 0){
			perror("accept");
			continue;
		}

		

		arg = (struct thread_arg*)malloc(sizeof(struct thread_arg));
		if(arg == NULL){
			perror("malloc");
			continue;
		}
		arg->sock = sock;

		if(pthread_create(&thread_id, NULL, handle_client, (void*)arg) != 0){
			perror("pthread");
			continue;
		}

/*---------------------------------------------------------------------------------*/
	}
	close(server_sock);
}



void* handle_client(void* arg){
	int running = 1;
	int  len;
	char buf[256];
	user user0;

	user0.sock = ((struct thread_arg*)arg)->sock;

	// read user name
	len = recv(user0.sock, buf, sizeof buf, 0);
	if(len < 0){
		perror("recv");
		return NULL;
	}
	user0.setname(buf);

	alluser.push_back(user0);
	printf("[LOGIN USER] %d %s \n", user0.id, user0.name);

	
	running = 1;
	while(running > 0){
		memset(buf, 0, sizeof buf);

		len = recv(user0.sock, buf, sizeof(buf), 0);
		if(len < 0){
			perror("recv");
			return NULL;
		}
		buf[len] = 0;
		printf("recv:[%s] \n", buf);
		
		len = send_members(alluser, user0.id, buf, strlen(buf));
		if(len < 0){
			perror("send");
			continue;
		}

	}

	close(((struct thread_arg*)arg)->sock);
	return NULL;
}


int send_members(std::vector<user> vec, int myid, const void* buf, int len){
	
	for(int i=0; i<vec.size(); i++){
		if(i == myid)
			continue;

		printf("send to %s \n", vec[i].name);
		len = send(vec[i].sock, buf, len, 0);
		if(len < 0){
			perror("send");
			continue;
		}
	}
	return len;
}



void chat(int sock0, int sock1){
	struct pollfd host[2];
	int ready, len;
	int ni;
	char buf[256];

	host[0].fd = sock0;
	host[0].events = POLLIN|POLLERR;
	host[1].fd = sock1;
	host[1].events = POLLIN|POLLERR;

	while(1){
		switch(ready=poll(host, 2, 100)){
			case -1:
				if(errno!=EINTR){
					perror("poll");	
				}
				break;
			case 0:
				break;
			default:
				for(int i=0; i<2; i++){
					if(host[i].revents & (POLLIN|POLLERR)){
						len = recv(host[i].fd, buf, sizeof buf, 0);
						if(len < 0){
							perror("recv");
							continue;
						}
						
						if(i == 0) ni = 1;
						else       ni = 0;

						len = send(host[(ni)].fd, buf, len, 0);
						if(len < 0){
							perror("send");
							continue;
						}
					}
				}
				break;
		}
	}

}







int get_user_list(std::vector<user> vec, char* buf, int len){
	memset(buf, 0, sizeof len);
	len = 0;
	char str[256];

	for(int i=0; i<vec.size(); i++){
		memset(str, 0, sizeof str);
		sprintf(str, "%d %s", vec[i].id, vec[i].name);
		len += strlen(str) + 1;
		strcat(buf, str);		
		strcat(buf, "\n");
	}

	return len;
}



