
# 中級プログラミングまとめ

14x3103 城倉 弘樹 [2016.02.20 最終更新]

試験は大問5つで90分だそう

先生が言った五カ条をいかに示す。

 - Containerのiterator
 - 特殊化、部分特殊化
 - 末尾再帰
 - デザインパターンのクラス図実装
 - リファクタリング






## 先生の予定

 - 火曜()  午後にいる
 - 水曜()  午後にいる
 - 木曜(2) 午後にいる




## Container のiterator

教材とコンテナクラスの実装が違う問題がでます。
イテレータを実装する問題が出る。 教材に答えが乗ってる。
イテレータ実装のやり方を全部覚えるしかない

とりあえず覚える必要のあるイテレータを以下に示す。

 - インクリメント演算子++ (前置)
 - インクリメント演算子++ (後置) (int引数)
 - 比較演算子==
 - 比較演算子!=
 - 実体参照演算子*
 - 代入演算子=


以下にサンプルプログラムを示す





### イテレータの種類

だいたい以下のようなイテレータがある。

 - 読み込み*
 - 書き込み*
 - 前置++
 - 後置++
 - 比較==
 - 比較!=

これらの実装例を以下に示す。


	#include <iostream>
	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	#include <string.h>


	class List {
			class Node {
				public:
					int head;
					Node* next;
					Node(int v, Node* n) : head(v), next(n) {}
			};
			Node* first;
		public:
			class iterator : std::forward_iterator_tag {
				public:
				Node* np;
				iterator(Node* p) : np(p) {}

				iterator& operator++(){
					np = np->next;
					return *this;
				}
				iterator operator++(int){
					iterator it = *this;
					np = np->next;
					return it;
				}
				int& operator*(){
					return np->head;	
				}
				bool operator==(const iterator& rhs) const {
					return rhs.np == np;
				}
				bool operator!=(const iterator& rhs) const {
					return !(rhs == *this);
				}
				iterator operator=(iterator& rhs) {
					np = rhs.np;
					return *this;
				}
			};

			iterator begin() { return iterator(first);}
			iterator end()   { return iterator(nullptr);}

			List() : first(nullptr) {}
			void push_back(int v) {
				Node* tmp = first;
				if(tmp == nullptr) {
					first = new Node(v, nullptr);	
					return ;
				}
				while(tmp->next != nullptr) {
					tmp = tmp->next;
				}
				tmp->next = new Node(v, nullptr);
			}
			void pop_back() {
				Node* tmp =	first;
				if(first == nullptr) {
					printf("aleady empty \n");
					return ;
				}
				if(first->next == nullptr) {
					delete(first);
					first = nullptr;
					return ;
				}
				while(tmp->next->next != nullptr){
					tmp = tmp->next;	
				}
				delete(tmp->next);
				tmp->next = nullptr;
			}
			size_t size() {
				Node* tmp = first;
				int count = 0;
				if (tmp == nullptr) {
					return 0;	
				}
				while(tmp != nullptr){
					tmp = tmp->next;
					count++;
				}
				return count;
			}
	};


	int main(){
		List l;
		l.push_back(1);
		l.push_back(2);
		l.push_back(3);
		l.push_back(4);
		printf("size: %d \n", (int)l.size());
		
		printf("------------------\n");
		for(List::iterator it = l.begin(); it != l.end(); ++it ) {
			printf("%d \n", *it);
		}
		printf("------------------\n");

		l.pop_back();
		l.push_back(12);

		for(List::iterator it = l.begin(); it != l.end(); ++it ) {
			printf("%d \n", *it);
		}
		printf("------------------\n");

	}





## テンプレートの特殊化、部分特殊化

### 特殊化

以下のようなテンプレート関数があるとする。

	template <typename T>
	void swap(T* a, T* b){
		T tmp = *a;
		*a = *b;
		*b = tmp;
	}


これをint型で特殊化すると以下のようになる。

	int a = 10;
	int b = 20;
	swap<int>(&a, &b);


### 部分特殊化


特殊化より、すこしトリッキーで柔軟

以下のようなクラスがあったとする


	class Male {
		public:
			static std::string like(){ return "female"; }
			static std::string me(){ return "male"; }
	};

	class Female {
		public:
			static std::string like(){ return "male"; }
			static std::string me(){ return "female"; }
	};

	class Japanese {
		public:
			static std::string country() { return "japan"; }
	};

	class French {
		public:
			static std::string country() { return "french"; }
	};
 
これらのクラスをつかったPersonクラスを以下に示す。


	template <class Sex, class Country>
	class Person {
		private:
		public:
			void hello() {
				std::cout << "Hi, I'm " << Sex::me() << ". So I like ";
				std::cout << Sex::like() << ". ";
				std::cout << "I'm from " << Country::country() << ".\n";
			}
	};


これを特殊化すると、以下のようになる。

	Person<Male, Japanese> p;
	p.hello();



ちなみに部分特殊化はこんな利点もある。サンプルコードを以下に示す。

	#include <iostream>
	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	#include <string.h>


	class window1 {
		public:
		static void window() { printf("dialog1 \n"); }
	};
	class window2 {
		public:
		static void window() { printf("dialog2 \n"); }
	};
	class controller1 {
		public:
		static void control() { printf("control1 \n"); }
	};
	class controller2 {
		public:
		static void control() { printf("control2 \n"); }
	};


	template <class Window, class Controller>
	class Widget {
		public:
		Widget() {
			Window::window();
			Controller::control();
		}
	};

	template <class Controller>
	class Widget<window1, Controller> {
		public:
		void info() {
			printf("This is BUBUN TOKUSYUKA 1\n");	
		}
	};

	template <class Contoroller>
	class Widget<window2, Contoroller> {
		public:
		void info() {
			printf("This is BUBUN TOKUSYUKA 2\n");	
		}
	};

	template <class Controller>
	class SP_Widget : public Widget<window1, Controller> {
		public: 
				
	};


	int main(){
		Widget<window1, controller1> w1;
		w1.info();

		SP_Widget<controller1> s;
		s.info();
	}




## 末尾再帰

これはプログラムの構造を覚えるしかない

### ファクトリアル(階乗)

#### 再帰
	
	int fact(int n) {
		if(n == 1 || n == 0) return 1;
		else return n * fact2(n-1);
	}

#### 末尾再帰

	int fact(int n, int r=1) {
		if (n == 0) return r;
		else return fact(n-1, r*n);
	}

### フィボナッチ数列

#### 再帰

	int fib(int n) {
		if(n == 0) return 1;
		else if(n == 1) return 1;
		else return fib2(n-1) + fib2(n-2);
	}

#### 末尾再帰

	int fib(int n, int fst=1, int scd=1){
		if (n == 0 || n == 1) return 1;
		int r = fst + scd;
		return n-2 ? fib(n-1, scd, r) : r;
	}


## クラス図   これは全覚え

クラスのメンバ構成およびクラス感の包含・親子関係を表す。

上から***クラス名***と***属性名***、セバレータ、***操作名***を書く

### アクセス修飾子

 
	  記号     可視正        意味                               
	  +        public        どこからでも可視                   
	  #        protected     当該クラスとその派生クラスのみ可視 
	  ~        package       同一パッケージ内からのみ可視       
	  -        private       当該クラスからのみ可視             




### 属性

自身のメンバを書く

 - ***staticメンバには下線を引いて示す。***
 - ***抽象クラス名、抽象メンバ名はイタリック表記をする***
 - ***継承などでオーバーロードしたメンバは書かなくていい***


### 関係


![](./img/dependency.gif) 
依存 (dependency)


![](./img/association.gif)
関連 (association)


![](./img/aggregation.gif)
集約 (aggregation)


![](./img/composition.gif)
合成 (composition)


![](./img/inheritance.gif)
継承 (inheritance)


### デザインパターン

以下を参照が一番手っ取り早い

http://palloc.hateblo.jp/entry/2016/01/19/013932


クラス図を使って以下を実装する
visitor observer
これ覚えて！って言った内容から覚えてきて、
クラス図書いたり、

visitor出るぞ！！(by 宮本)

#### Template Method Pattern

いままでの授業とかでよくあったやつ
FigureクラスからRectangleクラスとか、Circleクラスに継承していくやつ。

	class Figure
		virtual area()
		virtual print_area()

	class Rectangle : Figure
		area()
		print_area()

	class Circle : Figure
		area()
		print_area()


#### Singleton Pattern

オブジェクトの個数を最大でも1に制限して、どのインスタンスからもそれを参照するようにしたやつ
	
	class Singleton
		private:
			static _instance;
		public:
			static singleton* instance()
				if !(_instance)
					_instanc = new Singleton()
				return _instance;



#### State Pattern

オブジェクトの状態をクラスにして管理すること

#### Command Pattern

関数などの引数がたくさんになった時、それらをまとめて一つのクラスにして
関数に渡す。

#### Observer Pattern

複数のオベジェクトの管理をobserverクラスで行う。
setterなどの時に通知を行うことで、状態を監視する必要がなくなる利点がある。

#### Visitor Pattern

Don't think, feel.


	#include <iostream>
	#include <stdio.h>
	#include <string.h>


	class D;
	class E;


	class Visitor {
		public:
			virtual int visit(D* e) = 0;
			virtual int visit(E* e) = 0;
	};

	class ReturnVisitor : public Visitor {
		public:
			int visit(D* e);
			int visit(E* e);
	};


	class B{
		virtual int foo(Visitor* v)=0;	
	};


	class D : public B {
	public:
		int d;
		D(int p):d(p){}
		int foo(Visitor* v) { return v->visit(this); }
	};


	class E : public B {
	public:
		int e;
		E(int q):e(q){}
		int foo(Visitor* v) { return v->visit(this); }
	};


	int ReturnVisitor::visit(E* e) {return e->e;}
	int ReturnVisitor::visit(D* e) {return e->d;}


	int main() {
		D x(2);
		E y(3);

		ReturnVisitor pv ;

		std::cout << x.foo(&pv)<<"\n";
		std::cout << y.foo(&pv)<<"\n";
	}




## リファクタリング

これは各自やればいいっぽい。自分で考えて解く




## ファンクタ  (今回の範囲じゃない?)

クラスの()演算子をオーバーロードして、クラスを関数のように考える.
関数オブジェクトの一つ？てきな? (誰か教えて下さい。) 


	#include <iostream>

	class GreaterThan {
		public:
			bool operator()(int a, int b) {
				return a > b;	
			}
	};

	int main(int argc, char** argv){
		GreaterThan gt;
		
		if( gt(5, 9))	printf("yes\n");
		else 			printf("no\n");
	}


