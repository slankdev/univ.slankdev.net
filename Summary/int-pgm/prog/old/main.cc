#include <iterator>

template <class T>
class List {
	class Node {
		T head;
		Node* tail;
		Node(T h,Node* t) : head(h),tail(t) {}
		~Node() { delete tail; }
	};
	Node* first;
	public:
	class iterator : public std::iterator<std::forward_iterator_tag,T> {
		Node* np;
		iterator(Node* n) : np(n) {}
		public:
		iterator& operator++()
		{
			np = np->tail;
			return *this;
		}
		iterator operator++(int)
		{
			iterator it = *this;
			np = np->tail;
			return it;
		}
		bool operator==(const iterator& rhs) const
		{
			return np == rhs.np;
		}
		bool operator!=(const iterator& rhs) const
		{
			return !(*this == rhs);
		}
		T& operator*() const
		{
			return np->head;
		}
	};

	List() : first(nullptr){}
	iterator begin(){ return iterator(first);}
	iterator end(){ return iterator(nullptr);}
	// void push_front(T x);
	// void clear();
	// ~List() { clear();}
};



int main(){
	List<int> l;	
	



}


