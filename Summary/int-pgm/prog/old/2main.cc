#include <stdio.h>
#include <iostream>
#include <iterator>


class array {
	public:
		int value;
		array* prev;
		array* next;

		array(int v, array* p, array* n) : 
			value(v), prev(p), next(n) {}
		array() : value(999), prev(nullptr), next(nullptr) {}

		int size(int n=1) {
			if(next == nullptr){
				return n;	
			}else{
				return next->size(n+1);
			}
		}
	
		void push_back(int v) {
			if (next == nullptr)
				next = new array(v, this, nullptr);	
			else
				next->push_back(v);	
		}

		void pop_back() {
			if(this->next->next == nullptr){ 
				delete(next);
				next = nullptr;
			} else {
				next->pop_back();	
			}
		}

		void print() {
			printf("%d \n", value);
			if (next == nullptr) { 
				return ;
			} else {
				next->print();
			}
		}
};


class list {
	protected:
		array* first;
	public:
		class iterator : public std::iterator<std::forward_iterator_tag,int> {
			public:
				array* np;   
				iterator(array* n) : np(n) {}
				iterator& operator++() {
					np = np->next;
					return *this;
				}
				iterator operator++(int) {
					iterator it = *this;
					np = np->next;
					return it;
				}
				bool operator==(const iterator& rhs) const {
					return np == rhs.np;
				}
				bool operator!=(const iterator& rhs) const
				{
					return !(*this == rhs);
				}
				int& operator*() const
				{
					return np->value;
				}
		};

		iterator begin() { return iterator(first);   }
		iterator end()   { return iterator(nullptr); }

		list() {
			first = new array(999, nullptr, nullptr);
		}
		void init(int v){
			first = new array(v, nullptr, nullptr); 	
		}
		void push_back(int v) {
			first->push_back(v);
		}
		void pop_back() {
			first->pop_back();	
		}
		int size(){
			return first->size(); 
		}
		void print() {
			printf("----------\n");
			first->print();
			printf("----------\n");
		}
};


int main(){
	list l;

	l.init(0);
	for(int i=0; i<5; i++) {
		l.push_back(i+1); 
	}

	l.print();

	l.pop_back();
	l.pop_back();

	l.print();
}

