


#include <stdio.h>
#include <iostream>
#include <iterator>



class list {
	protected:
		class array {
			public:
				int value;
				array* prev;
				array* next;

				array(int v, array* p, array* n) : 
					value(v), prev(p), next(n) {}
		};
		array* first;
	public:
	
	


		list() {
			first = new array(999, nullptr, nullptr);
		}
		void init(int v){
			first = new array(v, nullptr, nullptr); 	
		}
		void push_back(int v) {
			array* tmp;
			tmp = first;
			while(1) {
				if (tmp == nullptr) {
					tmp = new array(v, nullptr, nullptr);
					return ;
				} else if (tmp->next == nullptr) { 
					tmp->next = new array(v, tmp, nullptr);
					return ;
				} else {
					tmp = tmp->next;	
				}
			}
		}
		void pop_back() {
			array* tmp;
			tmp = first;
			while(1) {
				if (tmp->next == nullptr) { 
					tmp->prev->next = nullptr;
					delete(tmp);
					return ;
				} else {
					tmp = tmp->next;	
				}
			}
		}
		int size(){
			int count = 0;
			array* tmp = first;
			while(tmp != nullptr) {
				tmp = tmp->next;	
				count ++;
			}
			return count; 
		}
		void print() {
			printf("size: %d ", size());
			array* tmp = first;
			
			printf("[");
			while(tmp != nullptr) {
				printf("%d ", tmp->value);
				tmp = tmp->next;
			}printf("] \n");
		}
};


int main(){
	list l;

	l.init(0);
	for(int i=0; i<5; i++) {
		l.push_back(i+1); 
	}

	l.print();

	l.pop_back();
	l.pop_back();

	l.print();
}

