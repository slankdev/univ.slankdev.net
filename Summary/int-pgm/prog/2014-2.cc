#include <iostream>
#include <stdio.h>
#include <string.h>


class D;
class E;


class Visitor {
	public:
		virtual int visit(D* e) = 0;
		virtual int visit(E* e) = 0;
};

class ReturnVisitor : public Visitor {
	public:
		int visit(D* e);
		int visit(E* e);
};


class B{
	virtual int foo(Visitor* v)=0;	
};


class D : public B {
public:
	int d;
	D(int p):d(p){}
	int foo(Visitor* v) { return v->visit(this); }
};


class E : public B {
public:
	int e;
	E(int q):e(q){}
	int foo(Visitor* v) { return v->visit(this); }
};


int ReturnVisitor::visit(E* e) {return e->e;}
int ReturnVisitor::visit(D* e) {return e->d;}


int main() {
	D x(2);
	E y(3);

	ReturnVisitor pv ;

	std::cout << x.foo(&pv)<<"\n";
	std::cout << y.foo(&pv)<<"\n";
}


