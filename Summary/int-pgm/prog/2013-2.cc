
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <list>

#define M_PI 3.14


class Rectangle;
class Circle;


class Visitor {
	public:
		virtual double visit(Rectangle* e) = 0;
		virtual double visit(Circle* e) = 0;

};

class AreaVisitor : public Visitor {
	public:
		double visit(Rectangle* e);
		double visit(Circle* e) ;
};



class Figure {
	public:
		virtual double area(Visitor* v)=0;
		void print_area(Visitor* v) { std::cout << area(v) <<"\n"; }
};


class Rectangle : public Figure{
	public:
		double width;
		double height;
		Rectangle(double w,double h) : width(w), height(h) {}
		double area(Visitor* v) { return v->visit(this); }
};


class Circle : public Figure {
	public:
		double radius;
		Circle(double r) : radius(r) {}
		double area(Visitor* v) { return v->visit(this); }
};


double AreaVisitor::visit(Rectangle* e){
	return e->width * e->height;
}


double AreaVisitor::visit(Circle* e){
	return M_PI * e->radius * e->radius;
}




int main()
{
	AreaVisitor v;

	Rectangle r(2.5,3.1);
	Circle c(2.9);
	Figure* p;
	p = &r;
	p->print_area(&v);
	p = &c;
	p->print_area(&v);
}


