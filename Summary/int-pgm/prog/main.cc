#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>




class List {
        class Node {
            public:
                int head;
                Node* next;
                Node(int v, Node* n) : head(v), next(n) {}
        };
        Node* first;
    public:
        class iterator : std::forward_iterator_tag {
            public:
            Node* np;
            iterator(Node* p) : np(p) {}

            iterator& operator++(){
                np = np->next;
                return *this;
            }
            iterator operator++(int){
                iterator it = *this;
                np = np->next;
                return it;
            }
            int& operator*(){
                return np->head;    
            }
            bool operator==(const iterator& rhs) const {
                return rhs.np == np;
            }
            bool operator!=(const iterator& rhs) const {
                return !(rhs == *this);
            }
            iterator operator=(iterator& rhs) {
                np = rhs.np;
                return *this;
            }
        };

        iterator begin() { return iterator(first);}
        iterator end()   { return iterator(nullptr);}

        List() : first(nullptr) {}
        void push_back(int v) {
            Node* tmp = first;
            if(tmp == nullptr) {
                first = new Node(v, nullptr);   
                return ;
            }
            while(tmp->next != nullptr) {
                tmp = tmp->next;
            }
            tmp->next = new Node(v, nullptr);
        }
        void pop_back() {
            Node* tmp = first;
            if(first == nullptr) {
                printf("aleady empty \n");
                return ;
            }
            if(first->next == nullptr) {
                delete(first);
                first = nullptr;
                return ;
            }
            while(tmp->next->next != nullptr){
                tmp = tmp->next;    
            }
            delete(tmp->next);
            tmp->next = nullptr;
        }
        size_t size() {
            Node* tmp = first;
            int count = 0;
            if (tmp == nullptr) {
                return 0;   
            }
            while(tmp != nullptr){
                tmp = tmp->next;
                count++;
            }
            return count;
        }
};




int main(){
    List l;
    l.push_back(1);
    l.push_back(2);
    l.push_back(3);
    l.push_back(4);

    printf("------------------\n");
    for(List::iterator it = l.begin(); it != l.end(); ++it ) {
        printf("%d \n", *it);
    }
    printf("------------------\n");


}




