#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <list>

class StringElement;
class IntElement;

class Visitor {
	public:
		virtual int visit(StringElement* e) = 0;
		virtual int visit(IntElement* e) = 0;
};

class PrintVisitor : public Visitor {
	public:
		int visit(StringElement* e);
		int visit(IntElement* e);
};

class Element {
	public:
		virtual int accept(Visitor* v) = 0;
};

class StringElement : public Element {
	public:
		std::string data;
		StringElement(std::string s) : data(s) {}
		int accept(Visitor* v) { return v->visit(this); }
};

class IntElement : public Element {
	public: 
		int value;
		IntElement(int v) : value(v) {}
		int accept(Visitor* v) {return v->visit(this); }
};


int PrintVisitor::visit(StringElement* e) {std::cout << e->data << std::endl; return 0;}
int PrintVisitor::visit(IntElement* e) {std::cout << e->value << std::endl; return 0;}


int main(){
	using namespace std;
	PrintVisitor pv;

	Element* b = new IntElement(3);
	b->accept(&pv);

}
