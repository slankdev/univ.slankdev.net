/******************************************************************************
       unsharp.c
       アンシャープマスクによる輪郭強調を行う。
	   2013/9/11　　作成　馬路　
	   10/10 出力画像をunsigned char型に変更
	   2015/10/28	Hiroki Shirokura (14x3103) some code fix, support error
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE 128
#define INFILE "trim.raw"
#define OUTFILE_RAW "unsharp_trim.raw"
#define BETA 1.0


int main()
{
	printf("input : %s \n", INFILE);
	printf("output: %s \n", OUTFILE_RAW);
	printf("beta  : %f \n", BETA);

    unsigned char	f1[ SIZE ][ SIZE ];///f1配列を入力画像とする
    int				i , j;
    double			f[ SIZE ][ SIZE ];//処理用の入力画像
    double			g[ SIZE ][ SIZE ];//強調画像
    double			l[ SIZE ][ SIZE ];//ぼけ画像
	unsigned char		g1[ SIZE ][ SIZE ];/*出力画像*/
    FILE			*fp1 , *fp2;
    
    fp1 = fopen(INFILE , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f1 , 1 , SIZE * SIZE , fp1 ) != SIZE * SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );


    for(i=0; i<SIZE; i++){
        for(j=0; j<SIZE; j++){
            f[i][j]=(double)f1[i][j];  //処理用の入力画像はf配列を用いること
			g[ i ][ j ] = l[ i ][ j ] = 0;
        }
    }
    /*************アンシャープマスク処理：trim画像****************/


    /*************アンシャープマスク処理(trim)：ここまで****************/
	for( i = 0 ; i < SIZE ; i++ )
        for( j = 0 ; j < SIZE ; j++ )
			g1[i][j]=g[i][j];



    fp2=fopen(OUTFILE_RAW , "wb" );
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite( g1 , sizeof(unsigned char) , SIZE * SIZE, fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );

}

