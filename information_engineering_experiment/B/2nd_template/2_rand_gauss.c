/******************************************************************************
	1_rand_gauss.c
	ガウス雑音のヒストグラムを作成

	2013/9/11　1_rand.cのコピー　馬路
		 9/27　出力形式をcsvに
		 　　　ヒストグラムの配列を拡大
			　 乱数用の変数r,sumを追加

	2015/10/28 Hiroki Shirokura  (14x3103) some code fix, support error
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define OUTFILE "rand_g.csv"

int main()
{
	printf("output: %s \n", OUTFILE);

	int		i , k;
	int		j;
	double 	f[ 10000 ];////一様乱数を入れる配列 
	double  r , sum ;//一様乱数生成用
	int		g[ 200 ]; //ヒストグラム用配列    
	FILE	*fp1;
	
	srand( ( unsigned )time( NULL ) );
	memset(f, 0, sizeof f);
	memset(g, 0, sizeof g);


	
	/*************発生頻度の調査****************/
	

	/*************発生頻度の調査ここまで****************/
	

	fp1 = fopen( OUTFILE , "wb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}


	for( k = 0 ; k < 200 ; k++ )
		fprintf( fp1 , "%d\n" , g[ k ] );
	fclose( fp1 );
	
}

