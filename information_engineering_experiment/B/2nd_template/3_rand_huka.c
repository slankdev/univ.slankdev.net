/******************************************************************************
       	2_rand_huka.c
       	一様雑音を発生させ画素に付加する。
	   	2013/9/11　ファイルの出入力を簡略化　　差分画像の削除　馬路
			9/27　出力形式をcsvに
			　　　乱数用の変数rを追加
		2015/10/28 Hiroki Shirokura  (14x3103) some code fix, support error
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 128
#define INFILE "trim.raw"
#define OUTFILE_RAW "rand_huka.raw"
#define OUTFILE_CSV "rand_huka.csv"
#define PERCENT 0.3
#define MAX 30

int main(){
	printf("input : %s\n", INFILE);
	printf("output: %s\n", OUTFILE_RAW);
	printf("output: %s\n", OUTFILE_CSV);
	printf("hindo : %f \n", PERCENT);
	printf("max   : %d \n", MAX);

    unsigned char		f[ SIZE ][ SIZE ];//原画像はf配列に    
    int					i,j,k,l;
    int					temp;
	int					m[ 256 ]={0};//m配列はヒストグラム用
	double				r;
    unsigned char		g[ SIZE ][ SIZE ];//g配列には雑音を負荷した画像を
    FILE				*fp1 , *fp2;     

    srand( ( unsigned )time( NULL ) );   
	memset(f, 0, sizeof f);
	memset(m, 0, sizeof m);
	memset(g, 0, sizeof g);


    fp1 = fopen(INFILE , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f , 1 , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

    /*************雑音負荷処理：一様雑音****************/


    /*************雑音負荷処理ここまで****************/


    fp2=fopen(OUTFILE_RAW, "wb");
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite( g ,sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );

    /*************ヒストグラム作成:雑音負荷画像(g配列)****************/


    /*************ヒストグラム作成ここまで****************/

    fp2 = fopen(OUTFILE_CSV , "w" );
	if(fp2 == NULL){
		perror("foepn");
		return -1;
	}
    for( i = 0 ; i < 256 ; i++ )
        fprintf( fp2 , "%d\n" , m[i] );

    fclose( fp2 );
}

