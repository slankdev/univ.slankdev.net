/******************************************************************************
	1_rand.c
	一様乱数のヒストグラムを作成
	
	2012/11/01　（柳瀬）
	・完成	   
	2013/9/11　ファイルの出入力を簡略化　馬路
		 9/27　出力形式をcsvに 
		 　　　乱数用の変数rを追加
	2015/10/28	Hiroki Shirokura (14x3103) some code fix, support error
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define OUTFILE "rand.csv"

int main()
{	
	printf("output: %s \n", OUTFILE);
	
	int		i , j , k;
	double  r;//一様乱数生成用
	double 	f[ 10000 ];////一様乱数を入れる配列 
	int		g[ 100 ]; //ヒストグラム用配列    
	FILE	*fp1;
	
	srand( ( unsigned )time( NULL ) );
	memset(f, 0, sizeof f);
	memset(g, 0, sizeof g);

	
	/*************発生頻度の調査****************/
	

	/*************発生頻度の調査ここまで****************/
	
	fp1 = fopen(OUTFILE , "wb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}

	for( k = 0 ; k < 100 ; k++ )
		fprintf( fp1 , "%d\n" , g[ k ] );

	fclose( fp1 );
}

