/******************************************************************************
       6_prewitt.c
       prewittオペレータを画像に適用する。
       
       2012/11/08　柳瀬　変数double a,bを追加
	   2013/9/11　ファイルの出入力を簡略化　各種雑音追加　馬路
			9/23  ファイル出力に関するエラーの修正
			10/10 出力画像をunsigned char型に変更
	   2015/10/28 Hiroki Shirokura  (14x3103) some code fix, support error
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 128
#define INFILE1 "trim.raw"
#define INFILE2 "rand_huka.raw"
#define INFILE3 "gauss_noise.raw"
#define INFILE4 "wb_noise.raw"   
#define OUTFILE1 "prewitt_trim.raw"
#define OUTFILE2 "prewitt_noise.raw"
#define OUTFILE3 "prewitt_gauss.raw"
#define OUTFILE4 "prewitt_wb.raw"


int main()
{	
	printf("%s   ->    %s \n", INFILE1, OUTFILE1);
	printf("%s   ->    %s \n", INFILE2, OUTFILE2);
	printf("%s   ->    %s \n", INFILE3, OUTFILE3);
	printf("%s   ->    %s \n", INFILE4, OUTFILE4);

    unsigned char	f1[ SIZE ][ SIZE ];///f配列を入力画像とする
    int				i , j , k , l , m , t;
    double			a , b;
    double			f[ SIZE ][ SIZE ];/*処理する入力画像*/
    double			g[ SIZE ][ SIZE ];  ///結果画像 
	unsigned char	g1[ SIZE ][ SIZE ];/*出力画像*/
    FILE			*fp1,*fp2;

    fp1 = fopen( INFILE1 , "rb" );
	if(fp1 == NULL){
		perror("fopen1");
		return -1;
	}
    if(fread( f1 , 1 , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

   for( i = 0 ;  i < SIZE ; i++ )
   {
        for( j = 0 ; j < SIZE ; j++ )
        {
            f[ i ][ j ] = ( double )f1[ i ][ j ];
            g[ i ][ j ] = 0;
        }
   }
   
   /*************prewittフィルタ処理：trim画像****************/
   

   /*************prewittフィルタ処理(trim)：ここまで****************/

   for( i = 0 ; i < SIZE ; i++ )
        for( j = 0 ; j < SIZE ; j++ )
			g1[i][j]=g[i][j];

	fp2 = fopen(OUTFILE1, "wb" );
	if(fp2 == NULL){
		perror("fopen1");
		return -1;
	}
    if(fwrite( g1 , sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );




	fp1 = fopen(INFILE2 , "rb" );
	if(fp1 == NULL){
		perror("fopen2");
		return -1;
	}
    if(fread( f1 , 1 , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

	    for( i = 0 ; i < SIZE ; i++ )
    {
        for( j = 0 ; j < SIZE ; j++ )
        {
			f[ i ][ j ] = ( double )f1[ i ][ j ];
            g[ i ][ j ] = 0.0;
        }
    }

	/*************prewittフィルタ処理：一様雑音付加画像****************/


	/*************prewittフィルタ処理(一様雑音付加画像)：ここまで****************/

	for( i = 0 ; i < SIZE ; i++ )
		for( j = 0 ; j < SIZE ; j++ )
			g1[i][j]=g[i][j];


	fp2 = fopen(OUTFILE2, "wb" );
	if(fp2 == NULL){
		perror("fopen2");
		return -1;
	}
    if(fwrite( g1 , sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );

	fp1 = fopen(INFILE3 , "rb" );
	if(fp1 == NULL){
		perror("fopen3");
		return -1;
	}
    if(fread( f1 , sizeof( unsigned char ) , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

	    for( i = 0 ; i < SIZE ; i++ )
    {
        for( j = 0 ; j < SIZE ; j++ )
        {
			f[ i ][ j ] = ( double )f1[ i ][ j ];
            g[ i ][ j ] = 0.0;
        }
    }

	/*************prewittフィルタ処理：ガウス雑音付加画像****************/
   

    /*************prewittフィルタ処理(ガウス雑音付加画像)：ここまで****************/

   	for( i = 0 ; i < SIZE ; i++ )
		for( j = 0 ; j < SIZE ; j++ )
			g1[i][j]=g[i][j];
		

	fp2 = fopen(OUTFILE3 , "wb" );
	if(fp2 == NULL){
		perror("fopen3");
		return -1;
	}
    if(fwrite( g1 , sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );

	fp1 = fopen(INFILE4 , "rb" );
	if(fp1 == NULL){
		perror("fopen4")	;
		return -1;
	}
    if(fread( f1 , sizeof( unsigned char ) , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread")	;
		return -1;
	}
    fclose( fp1 );

	    for( i = 0 ; i < SIZE ; i++ )
    {
        for( j = 0 ; j < SIZE ; j++ )
        {
			f[ i ][ j ] = ( double )f1[ i ][ j ];
            g[ i ][ j ] = 0.0;
        }
    }
	/*************prewittフィルタ処理：ごましお雑音付加画像****************/
   

    /*************prewittフィルタ処理(ごましお雑音付加画像)：ここまで****************/

	for( i = 0 ; i < SIZE ; i++ )
     	for( j = 0 ; j < SIZE ; j++ )
			g1[i][j]=g[i][j];
		
	fp2 = fopen(OUTFILE4 , "wb" );
	if(fp2 == NULL){
		perror("fopen4");
		return -1;
	}
    if(fwrite( g1 , sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );


}

