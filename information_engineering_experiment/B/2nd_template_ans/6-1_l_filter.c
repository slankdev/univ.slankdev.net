/******************************************************************************
       4_l_filter.c
       3×3の均一線形フィルタリングを雑音画像に実行する。
	   2013/9/11　ファイルの出入力を簡略化　ボケ画像削除　馬路
	        9/27  入力画像と処理画像用の配列を追加
			10/10 出力画像をunsigned char型に変更
	   2015/10/28	Hiroki Shirokura (14x3103) some code fix, support error
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE 128
#define INFILE1 "rand_huka.raw"
#define INFILE2 "gauss_noise.raw"
#define INFILE3 "wb_noise.raw"
#define OUTFILE1 "remove_noise.raw"
#define OUTFILE2 "remove_gauss_noise.raw"
#define OUTFILE3 "remove_wb_noise.raw"


int main()
{	
	
	printf("%s   ->    %s \n", INFILE1, OUTFILE1);
	printf("%s   ->    %s \n", INFILE2, OUTFILE2);
	printf("%s   ->    %s \n", INFILE3, OUTFILE3);

    int					i , j;
    char unsigned		f1[ SIZE ][ SIZE ];/*入力画像*/
    double				g[ SIZE ][ SIZE ];/*出力画像*/
	double				f[ SIZE ][ SIZE ];/*処理する入力画像*/
    double				r;
	unsigned char		g1[ SIZE ][ SIZE ];/*出力画像*/
    FILE            *fp1,*fp2;     


    fp1 = fopen(INFILE1 , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f1 , sizeof( unsigned char ) , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );


    for( i = 0 ; i < SIZE ; i++ )
    {
        for( j = 0 ; j < SIZE ; j++ )
        {
			f[ i ][ j ] = ( double )f1[ i ][ j ];
            g[ i ][ j ] = 0.0;
        }
    }

    /*************雑音除去1(一様雑音)：3×3線形フィルタ作成(重み付けなし)****************/


    /*************雑音除去1(一様雑音)：ここまで****************/

	for( i = 0 ; i < SIZE ; i++ )
        for( j = 0 ; j < SIZE ; j++ )
			g1[i][j]=g[i][j];

 	fp2 = fopen(OUTFILE1 , "wb" );
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite( g1 , sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose(fp2);

    fp1 = fopen(INFILE1 , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f1 , sizeof( unsigned char ) , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

    for( i = 0 ; i < SIZE ; i++ )
    {
        for( j = 0 ; j < SIZE ; j++ )
        {
			f[ i ][ j ] = ( double )f1[ i ][ j ];
            g[ i ][ j ] = 0.0;
        }
    }

    /*************雑音除去1(ガウス雑音)：3×3線形フィルタ作成(重み付けなし)****************/


    /*************雑音除去1(ガウス雑音)：ここまで****************/    

    for( i = 0 ; i < SIZE ; i++ )
        for( j = 0 ; j < SIZE ; j++ )
			g1[i][j]=g[i][j];

 	fp2 = fopen(OUTFILE2 , "wb" );
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite( g1 , sizeof( unsigned char ), SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose(fp2);


 	fp1 = fopen(INFILE3 , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f1 , sizeof( unsigned char ) , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

    for(i = 0 ; i < SIZE ; i++ )
    {
        for(j = 0 ; j < SIZE ; j++)
        {
			f[ i ][ j ] = ( double )f1[ i ][ j ];
            g[ i ][ j ] = 0.0;
        }
    }

    /*************雑音除去1(ごま塩雑音)：3×3線形フィルタ作成(重み付けなし)****************/


    /*************雑音除去1(ごま塩雑音)：ここまで****************/

	for( i = 0 ; i < SIZE ; i++ )
       for( j = 0 ; j < SIZE ; j++ )
			g1[i][j]=g[i][j];

  	fp2 = fopen(OUTFILE3 , "wb" );
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite( g1 , sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );

}
