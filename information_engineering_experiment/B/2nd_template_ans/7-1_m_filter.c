/******************************************************************************
       5_m_filter.c
       3×3及び5×5の非線形フィルタリングを雑音画像に実行する。
	   2013/9/11　ファイルの出入力を簡略化　qsort関数削除　馬路
	   10/10 変数をunsigned char型に変更
	   2015/10/28	Hiroki Shirokura (14x3103) some code fix, support error
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE 128
#define INFILE1 "rand_huka.raw"
#define INFILE2 "gauss_noise.raw"
#define INFILE3 "wb_noise.raw"   
#define OUTFILE1 "remove_m_noise.raw"
#define OUTFILE2 "remove_m_gauss_noise.raw"
#define OUTFILE3 "remove_m_wb_noise.raw"


int main()
{
	printf("%s   ->    %s \n", INFILE1, OUTFILE1);
	printf("%s   ->    %s \n", INFILE2, OUTFILE2);
	printf("%s   ->    %s \n", INFILE3, OUTFILE3);

    int				i,j,k,l,m,t;
    unsigned char 	f[ SIZE ][ SIZE ];/*入力画像*/
    unsigned char	g[ SIZE ][ SIZE ];/*出力画像*/
    unsigned char			temp[ 9 ];   
    FILE			*fp1,*fp2;     

    fp1 = fopen(INFILE1  , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f , sizeof( unsigned char ), SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

    for( i = 0 ; i < SIZE ; i++)
    {
        for( j = 0 ; j < SIZE ; j++)
        {
            g[ i ][ j ] = 0.0;
        }
    }

    /*************雑音除去3(一様雑音)：3×3メディアンフィルタ作成****************/
    
    
    /*************雑音除去3(一様雑音)：ここまで****************/
     
 	fp2 = fopen(OUTFILE1 , "wb" );
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite( g , sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose(fp2);

    fp1 = fopen(INFILE1 , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f , sizeof( unsigned char ) , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

    for( i = 0 ; i < SIZE ; i++ )
    {
        for( j = 0 ; j < SIZE ; j++ )
        {
            g[ i ][ j ] = 0.0;
        }
    }
    
	/*************雑音除去3(ガウス雑音)：3×3メディアンフィルタ作成****************/


    /*************雑音除去3(ガウス雑音)：ここまで****************/
 	
 	fp2 = fopen(OUTFILE2 , "wb" );
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite(g,sizeof(unsigned char),SIZE*SIZE,fp2) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose(fp2);



 	fp1 = fopen(INFILE3 , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f , sizeof( unsigned char ) , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

    for(i = 0 ; i < SIZE ; i++ )
    {
        for(j = 0 ; j < SIZE ; j++)
        {
            g[ i ][ j ] = 0.0;
        }
    }

    /*************雑音除去3(ごま塩雑音)：3×3メディアンフィルタ作成****************/


    /*************雑音除去3(ごま塩雑音)：ここまで****************/

  	fp2 = fopen(OUTFILE3 , "wb" );
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite( g , sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );

}

