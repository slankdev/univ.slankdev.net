/******************************************************************************
       2_rand_huka_gauss.c
       一様雑音を発生させ画素に付加する。
	   2013/9/11　2_rand_huka.cのコピー　　馬路
		    9/27　出力形式をcsvに
			      乱数用の変数r,sumを追加
	   2015/10/28 Hiroki Shirokura  (14x3103) some code fix, support error
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 128
#define INFILE "trim.raw"
#define OUTFILE_RAW "gauss_noise.raw"
#define OUTFILE_CSV "gauss_noise_hist.csv"
#define PERCENT 0.3
#define STD 25

int main()
{
	printf("input : %s\n", INFILE);
	printf("output: %s\n", OUTFILE_RAW);
	printf("output: %s\n", OUTFILE_CSV);
	printf("hindo : %f \n", PERCENT);
	printf("stdarg: %d \n", STD);

    unsigned char		f[ SIZE ][ SIZE ];//原画像はf配列に    
    int					i , j , k , l;
    int					temp;
	double				r , sum;
	int					m[ 256 ]={0};//m配列はヒストグラム用
    unsigned char		g[ SIZE ][ SIZE ];//g配列には雑音を負荷した画像を
    FILE				*fp1 , *fp2;     

    srand( ( unsigned )time( NULL ) );   

    fp1 = fopen(INFILE , "rb" );
	if(fp1 == NULL){
		perror("fopen");
		return -1;
	}
    if(fread( f , 1 , SIZE * SIZE , fp1 ) != SIZE*SIZE){
		perror("fread");
		return -1;
	}
    fclose( fp1 );

    /*************雑音負荷処理：一様雑音****************/

	for(i=0; i<SIZE; i++){
		for(j=0; j<SIZE; j++){
			g[i][j]=f[i][j];
		}
	}
		
	for(k=0; k<SIZE*SIZE*PERCENT; k++){
		i=(double)rand()/RAND_MAX*127;
		j=(double)rand()/RAND_MAX*127;
		
		sum=0;
		for( l = 0 ; l < 12 ; l++ ){
			sum += (double)rand()/RAND_MAX;
		}
		r=sum-6;
		r=r*STD;
		
		temp=f[i][j]+r;
		
		if(temp<0){
			temp=0;
		}
		if(temp>255){
			temp=255;
		}
		
		g[i][j]=temp;
	}

    /*************雑音負荷処理ここまで****************/

    fp2=fopen(OUTFILE_RAW,"wb");
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    if(fwrite( g ,sizeof( unsigned char ) , SIZE * SIZE , fp2 ) != SIZE*SIZE){
		perror("fwrite");
		return -1;
	}
    fclose( fp2 );

    /*************ヒストグラム作成:雑音負荷画像(g配列)****************/

	k=0;
    for(i=0; i<SIZE; i++){
		for(j=0; j<SIZE; j++){
			k=g[i][j];
			m[k]++;
		}
	}


    /*************ヒストグラム作成ここまで**********/

    fp2 = fopen(OUTFILE_CSV, "w" );
	if(fp2 == NULL){
		perror("fopen");
		return -1;
	}
    for( i = 0 ; i < 256 ; i++ )
        fprintf( fp2 , "%d\n" , m[i] );

    fclose( fp2 );

}

