/******************************************************************************
       1_trim.c
       画像を切り取る。
       一部修正　柳瀬　2012/11/01
	   ファイル出入力を削除　馬路　2013/9/11　尾川先生公認
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int             i, j;
	unsigned char   f[  ][  ]={};       /** 原画像用の配列 **/
    unsigned char   g[  ][  ];       /** トリミング後の画像の配列 **/
    FILE            *fp1 ,*fp2;

    /**原画像ファイル読み込み********************************/
    
    
    /****************************************************************/


    /**↓↓プログラム作成開始↓↓************************************/


    /**↑↑プログラム作成終了↑↑************************************/


    /**トリミング画像ファイル書き込み**************************************/
    fp2 = fopen( "trim.raw" , "wb" );
    fwrite( g , 1 ,  , fp2 );
    fclose( fp2 );
    
    /****************************************************************/

    return 0;
}

